<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
  protected $table= 'book' ;
  protected $primarykey = 'id_book';
  protected $fillable = array('id_book', 'author_id', 'title', 'publish_date'); 

  public funtion author()
 {
 	return $this->belongsTo('app\Author');
 }
}
