<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Author;

class AuthorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(['status'=>'ok','data'=>Author::all()], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id_author)
    {
        return "lista de autores $id_author";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $author = new Author;

        $author->name = $request->name;

        $author->save();

        if (!$request->input('id_author') || !$request->input('name') )
        {
            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan datos.'])],422);

        $nuevoAuthor=Author::create($request->all());
        $response = Response::make(json_encode(['data'=>$nuevoAuthor]), 201)->header('Location')->header('Content-Type', 'application/json');
        return $response;

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Author::where('id_author', $id_author)->get();

        if (!$author)
            {return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentra un autor con ese código.'])],404);
        }

        return response()->json(['status'=>'ok','data'=>$author],200);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
