<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id_book)
    {
        $book = new Book;

        $book->id_book = $request->id_book;
        $book->author_id = $request->author_id;
        $book->title = $request->title;
        $book->publish_date = $request->publish_date;

        $book->save();
         return response()->json(['status'=>'ok','data'=>Book::all()], 200);

         if (!$request->input('id_book') || !$request->input('author_id') || !$request->input('title') || !$request->input('publish_date'))
        {
            return response()->json(['errors'=>array(['code'=>422,'message'=>'Faltan datos.'])],422);

        $nuevoBook=Book::create($request->all());
        $response = Response::make(json_encode(['data'=>$nuevoBook]), 201)->header('Location')->header('Content-Type', 'application/json');
        return $response;

    }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
       //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $book = new Book;
        $book->id_book = $request->id_book;
        $book->author_id = $request->author_id;
        $book->title = $request->title;
        $book->publish_date = $request->publish_date;

        $book->save();

        return response()->json(['status'=>'ok','data'=>Book::all()], 200);
    }

    /**
     * Display the specified resource.
     *view('administracion.usuarios.index')->with('users', $users);
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         

        if (!$book)
            {return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentra libro con ese código.'])],404);
        }

        return Book::where('id_book', $id_book)->get()->whith('author',$author);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_book)
    {
       //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
