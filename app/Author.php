<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
   
  protected $table= 'author' ;
  protected $primarykey = 'id_author';
  protected $fillable = array('id_author', 'name'); 

  public funtion author()
 {
 	return $this->belongsTo('app\Book');
 }
}
