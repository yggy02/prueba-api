<?php

use Illuminate\Database\Seeder;
use App\Author;
use Faker\Factory as Faker;

class AuthorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker::create();

	for ($i=0; $i<4; $i++)
		{
			// Cuando llamamos al método create del Modelo Fabricante 
			// se está creando una nueva fila en la tabla.
			Author::create(
				[
					'id_author'=>$faker->word(),
					'name'=>$faker->word(),
				
				]
			);
		}
        
    }
}
