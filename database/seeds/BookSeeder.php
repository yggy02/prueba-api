<?php

use Illuminate\Database\Seeder;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

	for ($i=0; $i<4; $i++)
		{
			// Cuando llamamos al método create del Modelo Fabricante 
			// se está creando una nueva fila en la tabla.
			Book::create(
				[
					'id_book'=>$faker->word(),
					'author_id'=>$faker->word(),
					'title'=>$faker->word(),
					);
			        'publish_date'=>$faker->date(DD/MM/AAAA),
				
				]
			);
		}
    }
}
