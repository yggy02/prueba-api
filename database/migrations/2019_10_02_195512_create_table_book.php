<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBook extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('book',function(Blueprint $table)
        {
            $table->increments('id_book');
            $table->string('author_id');
            $table->string('title');
            $table->date('publish_date');
            $table->timestamps();

            $table->foregn('author_id')->references('id_author')->on('author');
           }); 
        
            }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('book');
    }
}
